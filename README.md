# soojale-maale

## to where one can fly from TLL

> Not to many places ;(

The Airports Council International (ACI) recognised TLL (Tallinn Airport) with the Best European Airport 2018 award. 
However if one will want to fly out from Tallinn ...

There are some  direct destinations from TLL, and there are some weekdays, when planes will take off. 
So **connection** it must be. 
To find connection there is many places to search, but all of them ask you **destination**. 
And if you have one in mind, then the answer is either: sorry, no flights or to many connections.

... So here you can get answer, what destinations are **'flyable'** 


```

    const startAirport = 'TLL'
    const startTime = new Date('2019-03-04')
    const endTime = new Date('2019-03-19')
    const plusminus = 1000 * 60 * 60 * 24 * 1
    const minConnection = 1000 * 60 * 60 * 2 
    const maxConnection = 1000 * 60 * 60 * 8
    
```

```

BCN - 2019-3-3 13:30:00 14:35:00 ( LGW 315 ) 19:50:00 22:55:00 [ BCN ] 2019-3-18 21:50:00 23:30:00 ( MXP 450 ) 07:00:00 11:05:00 TLL 16 147
SUF - 2019-3-3 12:15:00 14:20:00 ( MXP 305 ) 19:25:00 21:10:00 [ SUF ] 2019-3-18 21:20:00 23:05:00 ( MXP 475 ) 07:00:00 11:05:00 TLL 16 70
BCN - 2019-3-3 12:15:00 14:20:00 ( MXP 325 ) 19:45:00 21:15:00 [ BCN ] 2019-3-18 21:50:00 23:30:00 ( MXP 450 ) 07:00:00 11:05:00 TLL 16 116
BRI - 2019-3-3 12:15:00 14:20:00 ( MXP 325 ) 19:45:00 21:15:00 [ BRI ] 2019-3-18 21:45:00 23:20:00 ( MXP 460 ) 07:00:00 11:05:00 TLL 16 84


```